import matplotlib.pyplot as plt

class plotting:
    def __init__(self,data,no_of_clusters,mean) -> None:
        self.fig, self.ax = plt.subplots()
        self.data = data
        self.k = no_of_clusters
        self.mean = mean
    
    def show_plot():
        plt.show()
    
if __name__=='__main__':
    newplot = plotting([1,2,3],3,1)
    plotting.show_plot()
