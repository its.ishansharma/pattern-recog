import first as f
from pathlib import Path
import os

class files():
    def __init__(self, path):
        self.path = path
        self.file_count = self.filecounts()
        self.file_name = os.path.dirname(self.path)
        self.files = self.files_list()
        
    def filecounts(self):
        return (len(list(Path(self.path).glob('*.png'))))
       
    def files_list(self):
        return Path(self.path).glob('*.png')
    
files1 = files('data\cc\Test')
output_files_array = []

for file in files1.files:
    imag = f.Super_Image(str(file))
    output_files_array.append(imag)

