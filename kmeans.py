import main1
import random
import numpy as np
import pandas as pd

def random_select(data,frequency):
    random_vals = []
    for i in range(0,frequency):
        num = random.randint(0, len(data))
        random_vals.append(data[num])
    return (np.array(random_vals))

def euclidian_dist(xn, mean):
    return ((xn - mean) ** 2)

def kmeans(k,d,mean=np.array([])):  #k = no of clusters and d = main1.output_files_array[0]
    initial_random_mean = mean
    if mean.size == 0:
        print(f'selecting mean values randomly this means this is initial run')
        initial_random_mean = random_select(d.mean, k)
    output_dist_vector_all = []
    #new_mean_vector = np.delete(d.mean,initial_random_mean)
    for i in range(0,len(d.mean)):
        output_dist_vector=[]
        for j in range(0,len(initial_random_mean)):
            output_dist_vector.append(euclidian_dist(d.mean[i], initial_random_mean[j]))
        output_dist_vector_all.append(output_dist_vector.index(min(output_dist_vector)))
    df = pd.DataFrame({"image_mean": d.mean, "zk": output_dist_vector_all})
    return pd.get_dummies(df,columns=['zk'])

def kmeans_ultimate(k, d, mean):
    frame = kmeans(k, d,mean)
    first_mean = np.mean(frame[frame['zk_0'] == True]["image_mean"].to_numpy())
    second_mean = np.mean(frame[frame['zk_1'] == True]["image_mean"].to_numpy())
    third_mean = np.mean(frame[frame['zk_2'] == True]["image_mean"].to_numpy())
    new_mean = np.array([first_mean,second_mean,third_mean])
    return (new_mean)
    
if (__name__=='__main__') :
    no_of_clusters = 3
    data = main1.output_files_array[0]
    mean = kmeans_ultimate(no_of_clusters,data,np.array([]))
    for i in range(0,5):
        print(f'running {i+1} iteration')
        mean = kmeans_ultimate(no_of_clusters,data,mean)
        print(mean)

