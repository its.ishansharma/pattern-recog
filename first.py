import sys
import cv2
import numpy as np
np.set_printoptions(threshold=sys.maxsize)
import matplotlib.pyplot as plt
import os
import test

class Super_Image:
    def __init__(self,path):
        self.path = path
        self.file_name = os.path.basename(self.path)
        self.feature_file_name = os.path.splitext(self.path)[0] + ".npy"
        self.image = self.open_image_grayscale()
        self.pre_processed_image = self.preprocess_grayscale_image()
        #self.create_feature_vectors_file() #do not call this
        self.size = self.image.shape
        #self.data_sorting_stuff() #repeat code
        self.calculate_mean_var()

        
    def open_image_grayscale(self):
        print(f'opening {self.path} .. and creating image')
        image = cv2.imread(self.path, cv2.IMREAD_GRAYSCALE)
        return image

    def ret_file_name(self):
        return self.file_name
    
    def ret_feature_file_name(self):
        return self.feature_file_name
    
    def checkfileexists(self):
        return os.path.isfile(self.feature_file_name)

    def create_feature_vectors_file(self): #this is just writing image in file no processing
        print(f'creating feature vector file {self.feature_file_name}')
        np.savetxt(self.feature_file_name, self.image)

    def preprocess_grayscale_image(self):
        print(f'creating 7*7 array {self.feature_file_name}')
        self.final_data = test.writefile(self.feature_file_name, self.image)

    def ret_shape(self):
        return self.size

    def data_sorting_stuff(self):
        print(f'sorting 7*7 data properly for {self.path}')
        self.final_data=[]
        for i in range(0, len(self.created_77_array)):
            test = np.array(np.split(self.created_77_array[i], 7, axis=0))
            self.final_data.append(np.array(test))
        self.final_data = np.array(self.final_data)
        
    def calculate_mean_var(self):
        print(f'calculating mean and variance values for {self.path}')
        self.mean = []
        self.varis = []
        for i in range(0,len(self.final_data)):
            self.mean.append(np.mean(self.final_data[i]))
            self.varis.append(np.var(self.final_data[i]))
        self.mean = np.array(self.mean)
        self.varis = np.array(self.varis)
        

if __name__ == '__main__':
    image1 = Super_Image("data\cc\Test\/2.png")
    print(image1.created_77_array)
