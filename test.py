import sys
import numpy as np
np.set_printoptions(threshold=sys.maxsize)
rows = 512
cols = 512
matrix = np.zeros((rows, cols), dtype=float)
for i in range(rows):
    for j in range(cols):
        matrix[i, j] = (i + 1) * 10 + (j + 1)


def writefile(o_path, image):
    with open(o_path,'w') as f:
        patch_size = 7
        shift = 1
        patches = []
        for j in range(0, 512 - patch_size + 1, shift):
            for i in range(0, 512 - patch_size + 1, shift):
                # Extract a patch
                patch = image[i:i+patch_size, j:j+patch_size]
                patches.append(patch)
        patches_array = np.array(patches)
        patches_array = patches_array.reshape(-1, 7*7)
        
        final_data=[]
        for i in range(0, len(patches_array)):
            test = np.array(np.split(patches_array[i], 7, axis=0))
            final_data.append(np.array(test))
        final_data = np.array(final_data)
        
        np.save(o_path, final_data)
    return final_data   
        
if ( __name__=='__main__'):
    writefile("data\cc\Test\/2.txt", matrix)